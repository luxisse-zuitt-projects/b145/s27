db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);


// Aggregation Syntax

db.collections.aggregate(
	[
		{<stage 1>},
		{<stage 2>},
		{<stage 3>},
		...
	]
);

// Stages

// $match
	// Filters the documents to pass only the documents that match the specified condition(s) to the next pipeline stage.
		
	// Syntax:
		{ $match: {query} }


// Mini-activity
	// Group together documents that fruits are currently on sale

db.fruits.aggregate(
	[
		{$match: {"onSale": true} },
	]
);


// $count
	// Returnd a count of the number of documents at this stage of the aggregation pipeline.

	// syntax:
		{ $count: <string> }

// Mini-activity:
	// After grouping documents according to fruits currently on sale, count how many documents were returned by assigning it to fruitCount name

db.fruits.aggregate(
	[
		{$match: {"onSale": true} },
		{$count: "fruitCount" },
	]
);


// $group

	// syntax:

	{
  $group:
    {
      _id: <expression>, // Group By Expression
      <field1>: { <accumulator1> : <expression1> },
      ...
    }
 	}

// After grouping documents according to fruits currently on sale, group documents according to null, and get the total sum of the stock ("$stock") field. Then, assign it to "total" field (use $sum accumulator operator)

db.fruits.aggregate(
	[
		{$match: {"onSale": true} },
		{$group: 
			{
				_id: null, //group by null; we can group by _id by using: (_id: "$_id")
				total: {$sum: "$stock" }
			} 
		}
	]
);


// $project
	// Reshapes each document in the stream, such as by adding new fields or removing existing fields. For each input document, outputs one document.

	// syntax:
	{ $project: { <specification(s)> } }

// Mini-activity:
// group together fruits that has more than 10 stocks and then exclude id in the returning document

db.fruits.aggregate(
	[
		{ $match: {"stock": {$gt: 10}} },
		{ $project : { "_id": 0} }
	]
);


// $sort
	// syntax:
	{ $sort: { <field1>: <sort order>, <field2>: <sort order> ... } }

// Sort returned documents in ascending order according to price

db.fruits.aggregate(
	[
		{ $sort: { "price": 1 } }
	]
);


db.fruits.aggregate(
	[
		{ $match: {"stock": {$gt: 10}} },
		{ $project : { "_id": 0} },
		{ $sort: { "price": 1 } }
	]
);


// $unwind
	// Deconstructs an array field from the input documents to output a document for each element. Each output document replaces the array with an element value. For each input document, outputs n documents where n is the number of array elements and can be zero for an empty array.

	// syntax:
	{ $unwind: <field path> }


// deconstructs the array field
db.fruits.aggregate([ 
	{ $unwind : "$origin" } 
]);


// Mini-activity:
	// from the first stage $unwind, group together same origin and count how many documents are there in each origin

db.fruits.aggregate([ 
	{ $unwind : "$origin" },
	{
    $group: {
       _id: "$origin",
       kinds: {$sum: 1 }
    }
  }
]);